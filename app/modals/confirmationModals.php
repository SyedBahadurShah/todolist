 <!-- Delete Item Confirmation Modal -->
<div class="modal fade" id="deleteItemModal" tabindex="-1" role="dialog" aria-labelledby="deleteItemModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deleteItemModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
          <i class="fas fa-trash-alt fa-7x"></i><br /><br />
          <h4>Are You Sure?</h4>
          <p>You won't be able to revert this!</p>
        </center>
      </div>
      <div class="modal-footer">
        <center>
          <button type="button" id="yesDeleteModalButton" class="btn btn-sm btn-danger"><i class="fas fa-check-circle"></i> Yes, Delete It!</button>
          <button type="button" id="cancelDeleteModalButton" class="btn btn-sm btn-primary"><i class="fas fa-times-circle"></i> Cancel</button>
        </center>
      </div>
    </div>
  </div>
</div>