<?php
// Requiring App Config
require_once "../config/config.php";

// Add Item into List
if (isset($_POST['addItem']) && !empty(trim($_POST['addItem']))) {
	$itemName = trim($_POST['addItem']);
	$itemsOrderQuery = $database->prepare("SELECT itemID from items");
	$itemsOrderQuery->execute();
	$itemOrder = $itemsOrderQuery->rowCount() > 0 ? $itemsOrderQuery->rowCount() + 1 : 1;
	$userID = $_SESSION['userID'];
	$addItemQuery = $database->prepare("INSERT INTO items (userID, itemName, itemOrder, itemDate) VALUES (:userID, :itemName, :itemOrder, :itemDate)");
	$addItemQuery->execute([
		"userID" => $userID,
		"itemName" => $itemName,
		"itemOrder" => $itemOrder,
		"itemDate"	=>	date('Y-m-d H:i:s')
	]);

	$itemID = $database->lastInsertId();
	$response = array("success" => true, "itemID" => $itemID);
	echo json_encode($response);
}

// Mark as Done Item
if (isset($_POST['itemDone'])) {
	$itemID = $_POST['itemID'];
	$itemDone = ($_POST['itemDone'] == 1 ? 0 : 1);
	$itemDoneQuery = $database->prepare("UPDATE items SET itemDone = :itemDone WHERE itemID = :itemID");
	
	$itemDoneQuery->execute([
		"itemDone" => $itemDone,
		"itemID" => $itemID
	]);

	$response = ['success' => true, 'itemDone' => $itemDone];
	echo json_encode($response);
}

// Changing Item Color
if (isset($_POST['itemColor']) && !empty($_POST['itemColor'])) {
	$itemID = $_POST['itemID'];
	$itemColor = $_POST['itemColor'];
	$itemColorQuery = $database->prepare("UPDATE items SET itemColor = :itemColor WHERE itemID = :itemID");
	$itemColorQuery->execute([
		"itemColor" => $itemColor,
		"itemID" => $itemID
	]);
	$response = array("success" => true);
	echo json_encode($response);
}

// Deleting List Item 
if (isset($_POST['deleteItem']) && !empty($_POST['deleteItem'])) {
	$itemID = $_POST['deleteItem'];
	$itemDeleteQuery = $database->prepare("DELETE FROM items WHERE itemID = :itemID");
	$itemDeleteQuery->execute([
		"itemID" => $itemID
	]);
	$response = array("success" => true);
	echo json_encode($response);
}

// Updating List Item 
if (isset($_POST['updateItemName']) && !empty($_POST['updateItemName'])) {
	$itemID = $_POST['itemID'];
	$itemName = $_POST['updateItemName'];
	$itemNameUpdateQuery = $database->prepare("UPDATE items SET itemName = :itemName WHERE itemID = :itemID");
	$itemNameUpdateQuery->execute([
		"itemName" => $itemName,
		"itemID" => $itemID
	]);
	$response = array("success" => true);
	echo json_encode($response);
}

// Update itemOrders 
if (isset($_POST['listItem']) && !empty($_POST['listItem'])) {
	$itemsOrder = $_POST['listItem'];
	try {
		$database->beginTransaction();
		foreach($itemsOrder as $key => $value) {
			$itemOrderUpdateQuery = $database->prepare("UPDATE items SET itemOrder = :itemOrder WHERE itemID = :itemID");
			$itemOrderUpdateQuery->execute([
				'itemOrder' => $key,
				'itemID'	=> $value
			]);
		}
		$database->commit();
		$response = array("success" => true, "itemID" => $database->lastInsertId());
		echo json_encode($response);		
	} catch(PDException $error) {
		$database->rollback();
		echo "Error : " . $error->getMessage();
		$database = null;
	}
}
?>