// Mark as Done Item from To Do List
$(document).ready(function() { 
    $(document).on("click", ".markAsDone", function() {
       var itemID = $(this).data('id');
       var itemDone = $(this).data('done');
       var itemNameReference = $(".itemName", $(this).parents('li'));
       var itemDoneReference = $(this);
       $.ajax ({ 
            url: baseURL + 'app/server/server.php',
            data: {
              "itemID": itemID, 
              "itemDone": itemDone
            },
            type: 'post',
            dataType: 'json',
            success: function(result) {
                if (result['success']) {
                  if (result['itemDone'] == 1) {
                      $(itemNameReference).css({"text-decoration": "line-through"});
                      $(itemDoneReference).data('done', 1);
                      $(itemDoneReference).html('<i class="fas fa-check"></i>').fadeIn('fast');
                  } else {
                      $(itemNameReference).css({"text-decoration": "none"});
                      $(itemDoneReference).data('done', 0);
                      $(itemDoneReference).html('<i class="fas fa-minus"></i>').fadeIn('fast');
                  }
                }      
            }
       });
    });                                

    // Add Item to List
    $(document).on("click", "#addItem", function() {
      event.preventDefault();
      var itemName = $("#itemName").val();
      if (!itemName) {
        alert("You haven't added any list item yet!");
      } else { 
          var itemsListCount = $("#list li").length;
          if (itemsListCount == "0") {
            $("#list center").remove();
          }
          $.ajax ({
            url: baseURL + 'app/server/server.php',
            data: {"addItem": itemName},
            type: 'post',
            dataType: 'json',
            success: function(result) {
              if (result['success']) {
                var newItem = "\
                  <li id='listItem_" + result['itemID'] + "' class='list-group-item' style='display: none;'>\
                    <div class='row'>\
                      <div class='itemName col-sm-9'>" + itemName + "</div>\
                      <div class='col-sm-3'>\
                        <div class='btn-group' role='group'>\
                          <button type='button' class='changeOrder btn btn-sm btn-dark btn-small' data-toggle='tooltip' data-placement='top' title='Change Item for Order!'><i class='fas fa-expand-arrows-alt'></i></button>\
                          <button type='button' data-id='" + result['itemID'] + "' class='changeColor btn btn-sm btn-success btn-small' data-toggle='tooltip' data-placement='top' title='Change Color of Item!'><i class='fas fa-palette'></i></button>\
                          <button type='button' data-id='" + result['itemID'] + "' data-done='0' class='markAsDone btn btn-sm btn-primary' data-toggle='tooltip' data-placement='top' title='Mark Item as Done!'><i class='fas fa-minus'></i></button>\
                          <button type='button' data-id='" + result['itemID'] + "' class='deleteItem btn btn-sm btn-danger' data-toggle='tooltip' data-placement='top' title='Delete Item!'><i class='fas fa-times'></i></button>\
                        </div>\
                      </div>\
                    </div>\
                  </li>\
                ";

                $(newItem).appendTo($('#list')).slideDown("slow");
                $("#itemName").val(" ");
                $("#list").sortable("refresh");
              }
            }
          });                      
      }
  });   

  // Deleting Item from To Do List
  $(document).on("click", ".deleteItem", function() { 
    $('.tooltip').tooltip('hide'); // Hide Tooltip
    var itemID = $(this).data('id'); // Item ID
    var itemReference = $(this); // Item ID
    $("#deleteItemModal").modal("show"); // Showing Modal
    
    // Cancel Button - Delete Modal
    $(document).on("click", "#cancelDeleteModalButton", function() { 
        $("#deleteItemModal").modal("hide");
    });

    // Yes Button - Delete Modal
    $(document).on("click", "#yesDeleteModalButton", function() {
        $(itemReference).parents('li').slideUp("normal", function() {
          $(this).remove();
        });

        var itemsCount = $('#list li').length;
        if (!(itemsCount - 1) > 0) {
          $("#list").slideDown("slow", function() {
              $(this).html("<center>You haven't added any list item yet!</center>");
          });
        }

        // Sending Deleting Requuest to Server
        $.ajax ({ 
          url: baseURL + 'app/server/server.php',
          data: {"deleteItem": itemID},
          type: 'post'
        });

        // Hidding Delete Model Button
        $("#deleteItemModal").modal("hide");
    });

  });

  // Sortable List Items
  $("#list").sortable({
    cursor: "move",
    handle: ".changeOrder",
    cancel: '',
    refresh: '',
    update: function(event, ui) {
        var itemsOrder = $(this).sortable("serialize");
        $.ajax({ 
          type: "POST", 
          url: baseURL + 'app/server/server.php', 
          data: itemsOrder,
          dataType: 'json'
        }); 
    }
  });

  // Tooltip Handling
  $('[data-tooltip="tooltip"]').tooltip();

  $(document).on('dblclick', '.itemName', function() {
    var itemReference = $(this);
    var itemID = $(this).data('id');
    var itemName = $(this).text();  
    var editableBox = '<input type="text" id="editableBox" value="'+ itemName +'" />';  
    $(this).html(editableBox);
    $("#editableBox").select();

    //Bind Keypress Event to Document
    $(this).keypress(function(event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13') {
          var itemName = $("#editableBox").val(); 
          if ($.trim(itemName) === "") {
              alert("The item name can't be empty!");
          } else {
            $(itemReference).html(itemName);
            $.ajax ({ 
              url: baseURL + 'app/server/server.php',
              data: {"itemID": itemID, "updateItemName": itemName},
              type: 'post'
            });             
          }
        }
    });    
  });

  $(document).on('mouseenter', '.changeColor', function() {
    var pickerReference;
    $(".changeColor").ColorPicker({
        color: '#0000ff',
        onShow: function (colpkr) {
          pickerReference = this;
          $(colpkr).fadeIn(500);
          return false;
        },
        onHide: function (colpkr,btn) {
          $(colpkr).fadeOut(500);
          return false;
        },
        onChange: function (hsb, hex, rgb) {
            var itemReference = $('.itemName', $(pickerReference).parents('li'));
            var itemColor = '#' + hex; 
            var itemID = itemReference.data("id");
            // $('.itemName', $('.changeColor').parents('li'));
            // $('.changeColor').parents('li').find('.itemName');
            itemReference.css({"color": itemColor});
        },
        onSubmit: function (hsb, hex, rgb) {
            var itemReference = $('.itemName', $(pickerReference).parents('li'));
            var itemID = itemReference.data("id");
            var itemColor = '#' + hex;
            $.ajax ({ 
              url: baseURL + 'app/server/server.php',
              data: {"itemID": itemID, "itemColor": itemColor},
              type: 'post'
            }); 
        }    
    });
  });
});     