<?php
    require_once 'app/config/config.php';
    $itemsQuery = $database->prepare("SELECT * FROM items WHERE userID = :userID ORDER BY itemOrder");
    $itemsQuery->execute(['userID' => $_SESSION['userID']]);
    $items = $itemsQuery->rowCount() ? $itemsQuery : [];
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/all.css">
    <link rel="stylesheet" type="text/css" href="assets/css/stylesheet.css">
    <link rel="stylesheet" media="screen" type="text/css" href="assets/css/colorpicker.css" />
    <title>To Do List</title>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-sm-8" style="margin: auto;">
          <div class="card" style="margin-top: 10%;">
            <div class="card-header listHeader">
              <i class="fas fa-th-list"></i> &nbsp; To Do List
            </div>
            <div class="card-body">
              <ul id="list" class="list-group">
                <?php if (!empty($items)): ?>
                  <?php foreach($items as $item): ?>
                    <li id="listItem_<?php echo $item['itemID'] ?>" class="list-group-item">
                      <div class="row">
                        <div data-id="<?php echo $item['itemID']; ?>" class="itemName col-sm-9 <?php echo $item['itemDone'] == 1 ? ' done' : '' ?>" style="color:<?php echo $item['itemColor']; ?>;"><?php echo $item['itemName']; ?></div>
                        <div class="col-sm-3">
                          <div class="btn-group" role="group">
                              <button type="button" class="changeOrder btn btn-sm btn-dark btn-small" data-toggle="tooltip" data-placement="top" title="Change Order of Item!"><i class="fas fa-expand-arrows-alt"></i></button>
                              <button type="button" data-id="<?php echo $item['itemID']; ?>" class="changeColor chngclr btn btn-sm btn-success btn-small" data-toggle="tooltip" data-placement="top" title="Change Color of Item!""><i class="fas fa-palette"></i></button>
                              <button type="button" data-id="<?php echo $item['itemID']; ?>" data-done="<?php echo $item['itemDone']; ?>" class="markAsDone btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Mark Item as Done!"><i class="fas fa-minus"></i></button>
                              <button type="button" data-id="<?php echo $item['itemID']; ?>" class="deleteItem btn btn-sm btn-danger" data-tooltip="tooltip" data-placement="top" title="Delete Item from List!"><i class="fas fa-times"></i></button>
                           </div>
                        </div>
                      </div>
                    </li>  
                  <?php endforeach; ?>  
                <?php else: ?>
                    <center>You haven't added any list item yet!</center>
                <?php endif; ?>                            
              </ul>
              <form style="margin-top: 3%;">
                <div class="input-group">
                <input type="text" id="itemName" placeholder="Enter your new item!" autocomplete="off" class="form-control addNewListItem" autofocus required>
                  <div class="input-group-prepend">
                    <button id="addItem" class="btn btn-success addItem" name="addItem" type="submit">&nbsp;<i class="fas fa-plus-square"></i>&nbsp;</button>
                  </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require "app/modals/confirmationModals.php.php"; ?>
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript">
      var baseURL = '<?php echo $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] .$_SERVER['REQUEST_URI']; ?>';
      console.log(baseURL);
    </script>
    <script src="assets/js/custom-jquery.js"></script>
    <script src="assets/js/colorpicker.js"></script>
  </body>
</html>